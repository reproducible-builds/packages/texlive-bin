#!/usr/bin/env texlua  

print ("The pmx2pdf[.lua] script has been retired; please switch")
print ("to the musixtex script, which now processes .pmx files.")

